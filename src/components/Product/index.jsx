import { Component } from "react";
import "../Product/index.css";

class Product extends Component {
  render() {
    const { products, handleClick, filteredProducts } = this.props;

    return (
      <div className="cardProduct">
        {filteredProducts.length === 0
          ? products.map((item) => {
              return (
                <div className="product" key={item.id}>
                  <div>{item.category}</div>
                  <div className="productName">{item.name}</div>
                  <div className="productPrice">{item.price}</div>

                  <button onClick={() => handleClick(item.id)}>
                    Adicionar
                  </button>
                </div>
              );
            })
          : filteredProducts.map((item) => {
              return (
                <div className="product" key={item.id}>
                  <div>{item.category}</div>
                  <div className="productName">{item.name}</div>
                  <div className="productPrice">{item.price}</div>

                  <button onClick={() => handleClick(item.id)}>
                    Adicionar
                  </button>
                </div>
              );
            })}
      </div>
    );
  }
}
export default Product;
