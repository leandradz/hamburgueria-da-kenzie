import { Component } from "react";

class Input extends Component {
  state = {
    value: "",
  };

  render() {
    const { showProducts } = this.props;
    return (
      <form
        onSubmit={(event) => {
          event.preventDefault();
          this.setState({
            value: "",
          });
        }}
      >
        <input
          value={this.state.value}
          onChange={(event) => this.setState({ value: event.target.value })}
        />

        <button type="submit" onClick={() => showProducts(this.state.value)}>
          Pesquisar
        </button>
      </form>
    );
  }
}

export default Input;
