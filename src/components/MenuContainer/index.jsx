import { Component } from "react";
import Product from "../Product";
import Input from "../Input";
import CurrentSale from "../CurrentSale";

class MenuContainer extends Component {
  render() {
    const {
      products,
      handleClick,
      filteredProducts,
      showProducts,
      currentSale,
    } = this.props;

    return (
      <>
        <h1>Hamburgueria da Kenzie</h1>
        <Input products={products} showProducts={showProducts} />
        <Product
          products={products}
          filteredProducts={filteredProducts}
          handleClick={handleClick}
        />
        <CurrentSale currentSale={currentSale} />
      </>
    );
  }
}
export default MenuContainer;
