import { Component } from "react";

class CurrentSale extends Component {
  showSale = () => {
    const { currentSale } = this.props;
    return currentSale.saleDetails;
  };

  render() {
    const { currentSale } = this.props;
    return (
      <>
        <h2>Total da Compra: R$ {currentSale.total.toFixed(2)}</h2>

        <h2>Produtos Selecionados:</h2>
        <ul className="cardProduct">
          {currentSale.saleDetails.map((item) => (
            <li className="product">
              <li className="productSaleName">{item.name}</li>{" "}
              <li className="productSalePrice">R$ {item.price}</li>{" "}
              <li className="productSalecategory">{item.category}</li>{" "}
            </li>
          ))}
        </ul>
      </>
    );
  }
}

export default CurrentSale;
