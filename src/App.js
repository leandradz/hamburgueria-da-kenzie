import MenuContainer from "./components/MenuContainer";
import "./App.css";
import { Component } from "react";

class App extends Component {
  state = {
    products: [
      { id: 1, name: "Hamburguer", category: "Sanduíches", price: 7.99 },
      { id: 2, name: "X-Burguer", category: "Sanduíches", price: 8.99 },
      { id: 3, name: "X-Salada", category: "Sanduíches", price: 10.99 },
      { id: 4, name: "Big Kenzie", category: "Sanduíches", price: 16.99 },
      { id: 5, name: "Guaraná", category: "Bebidas", price: 4.99 },
      { id: 6, name: "Coca", category: "Bebidas", price: 4.99 },
      { id: 7, name: "Fanta", category: "Bebidas", price: 4.99 },
    ],
    filteredProducts: [],
    currentSale: { total: 0, saleDetails: [] },
  };

  handleClick = (productId) => {
    const { products, currentSale } = this.state;
    let productAdd = products.find((item) => item.id === productId);

    if (!currentSale.saleDetails.includes(productAdd)) {
      this.setState({
        currentSale: {
          total: currentSale.total + productAdd.price,
          saleDetails: [...currentSale.saleDetails, productAdd],
        },
      });
    }
  };

  showProducts = (inputValue) => {
    this.setState({
      filteredProducts: this.state.products.filter(
        (item) => item.name.toUpperCase() === inputValue.toUpperCase()
      ),
    });
  };

  render() {
    const { products, filteredProducts, currentSale } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <MenuContainer
            products={products}
            filteredProducts={filteredProducts}
            handleClick={this.handleClick}
            showProducts={this.showProducts}
            currentSale={currentSale}
          />
        </header>
      </div>
    );
  }
}

export default App;
